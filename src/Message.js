// @flow
import React, { useEffect } from "react";
import Markdown from "react-markdown";
import breaks from "remark-breaks";
import { formatDistance } from "date-fns";
import classnames from "classnames";
import type { ChatMessage } from "./Chatroom";
import { noop } from "./utils";

type MessageTimeProps = {
  time: number,
  isBot: boolean
};
export const MessageTime = ({ time, isBot }: MessageTimeProps) => {
  if (time === 0) return null;

  const messageTime = Math.min(Date.now(), time);
  const messageTimeObj = new Date(messageTime);
  return (
    <li
      className={classnames("time", isBot ? "left" : "right")}
      title={messageTimeObj.toISOString()}
    >
      {formatDistance(messageTimeObj, Date.now())}
    </li>
  );
};

type MessageProps = {
  chat: ChatMessage,
  onButtonClick?: (title: string, payload: string) => void,
  voiceLang?: ?string
};

const supportSpeechSynthesis = () => "SpeechSynthesisUtterance" in window;

const speak = (message: string, voiceLang: string) => {
  const synth = window.speechSynthesis;
  let voices = [];
  voices = synth.getVoices();
  const toSpeak = new SpeechSynthesisUtterance(message);
  toSpeak.voice = voices.find(voice => voice.lang === voiceLang);
  synth.speak(toSpeak);
};

const getMessageTitle = {
  "banda_larga": "Banda Larga",
  "movel": "Móvel",
  "fixo": "Fixo",
  "tv": "TV"
};

const getPlanDescription = (product, plan, offers) => {
  switch (product) {
    case "banda_larga":
      if (plan === "flex") {
        return "Velocidade varia de acordo com a demanda";
      }
      return "Fibra";

    case "movel":
      const includesBandaLarga = offers.find(o => o.product === "banda_larga");
      if (includesBandaLarga) {
        return "O dobro de dados pra você";
      }
      return "Individual";

    case "tv":
      if (plan === "sd") {
        return "sd";
      }
      return "hd";
    
    case "fixo":
      if (plan === "local") {
        return "Ligações ilimitadas para fixos de DDD 34";
      }
      return "Ligações ilimitadas para celular e fixo no Brasil todo";
  };
};

const getPreviousPrice = (price, discount) => price + (price * discount);

const Message = ({ chat, onButtonClick, voiceLang = null }: MessageProps) => {
  const message = chat.message;
  const isBot = chat.username === "bot";

  useEffect(() => {
    if (
      isBot &&
      voiceLang != null &&
      message.type === "text" &&
      supportSpeechSynthesis()
    ) {
      speak(message.text, voiceLang);
    }
  }, []);

  switch (message.type) {
    case "button":
      return (
        <ul className="chat-buttons">
          {message.buttons.map(({ payload, title, selected }) => (
            <li
              className={classnames("chat-button", {
                "chat-button-selected": selected,
                "chat-button-disabled": !onButtonClick
              })}
              key={payload}
              onClick={
                onButtonClick != null
                  ? () => onButtonClick(title, payload)
                  : noop
              }
            >
              <Markdown
                source={title}
                skipHtml={false}
                allowedTypses={["root", "break"]}
                renderers={{
                  paragraph: ({ children }) => <span>{children}</span>
                }}
                plugins={[breaks]}
              />
            </li>
          ))}
        </ul>
      );

    case "image":
      return (
        <li className={`chat ${isBot ? "left" : "right"} chat-img`}>
          <img src={message.image} alt="" />
        </li>
      );
    case "text":
      return (
        <li className={classnames("chat", isBot ? "left" : "right")}>
          <Markdown
            className="text"
            source={message.text}
            skipHtml={false}
            allowedTypses={[
              "root",
              "break",
              "paragraph",
              "emphasis",
              "strong",
              "link",
              "list",
              "listItem",
              "image"
            ]}
            renderers={{
              paragraph: ({ children }) => <span>{children}</span>,
              link: ({ href, children }) => (
                <a href={href} target="_blank">
                  {children}
                </a>
              )
            }}
            plugins={[breaks]}
          />
        </li>
      );
    case "custom":
      const payload = message.payload;
      return (
        <li className={classnames("chat", isBot ? "left" : "right")}>
          {payload.offers.map(o => (
            <div className="custom-message" key={o.product}>
              <span className="custom-minor">{getMessageTitle[o.product]}</span>
              <span className="custom-bigger">{o.plan}</span>
              <span className="custom-minor">{getPlanDescription(o.product, o.plan, payload.offers)}</span>
              <span className="custom-float">{o.discount * 100}% <br/> <label className="float-minor">DESCONTO</label></span>
            </div>
          ))}
          <span className="custom-outside">
            De  
            <label className="outside-low"> R${getPreviousPrice(payload.price, payload.discount).toFixed(2)} </label>
            por 
            <label className="outside-high"> R${payload.price.toFixed(2)}</label>
            /mês
          </span>
        </li>
      );
    default:
      return null;
  }
};

export default Message;
